# mobichar-app

## Description

test application for mobichar

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

```
## Endpoint

  Returns json with parsing data .

* **URL**

  /parser

* **Method:**

  `Post`
  
*  **Form data**
    `file`: `file with csv in zip format`

  Returns json with parsing data and save file in folder.

* **URL**

  /parser/save

* **Method:**

  `Post`
  
*  **Form data**
    `file`: `file with csv in zip format`

