import {NestFactory} from '@nestjs/core';
import { AppModule } from './modules/app.module';
import express = require('express');
import bodyParser = require('body-parser');
import * as fileUpload from 'express-fileupload';
import * as cors from 'cors';
import * as path from 'path';

async function bootstrap() {
  const server = express();
  const app = await NestFactory.create(AppModule, server);

  server.use('/temp', express.static(path.join(__dirname, '..', 'temp')));
  app.use(cors({
    origin: true,
    credentials: true,
  }));
  app.use(fileUpload());
  app.use(bodyParser.json());
  await app.listen(3000);
}
bootstrap();
