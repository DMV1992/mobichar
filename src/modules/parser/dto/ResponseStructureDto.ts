import * as moment from 'moment';

export class ResponseStructureDto {
    constructor(entity) {
      this.name = `${entity.first_name} ${entity.last_name}`;
      this.phone = entity.phone.replace(/\D/g, '');
      this.person = {
        firstName: entity.first_name,
        lastName: entity.last_name,
      };
      this.amount = entity.amount;
      const normalizeDate = entity.date.replace(/\//g, '-');
      this.date = moment(normalizeDate, 'DD-MM-YYYY').format('YYYY-MM-DD');
      this.costCenterNum = entity.cc.replace(/^(ACN)/, '');
    }
    name: string;
    phone: string;
    person: object;
    amount: number;
    date: string;
    costCenterNum: string;
  }