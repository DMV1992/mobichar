import { Module } from '@nestjs/common';
import { ParserService } from './parser.service';
import { ParserController } from './parser.controller';

@Module({
  imports: [],
  controllers: [ParserController],
  providers: [ParserService],
})
export class ParserModule {}
