import { Get, Controller, Req, HttpException, HttpStatus, Post } from '@nestjs/common';
import { ParserService } from './parser.service';
import _ = require('lodash');

@Controller('parser')
export class ParserController {
  constructor(private readonly parserService: ParserService) {}

  @Post()
  async root(@Req() req) {
    try {
        if (!(req.files && req.files.file)) {
            throw new HttpException('Invalid content (expected multipart/form-data [file] input)', HttpStatus.BAD_REQUEST);
          }
        const json = await this.parserService.readAndParseFile(req.files.file.data);
        return json;
    } catch (e) {
        console.log(e);
        throw e;
    }
  }

  @Post('/save')
  async saveAndReturn(@Req() req) {
    try {
        if (!(req.files && req.files.file)) {
            throw new HttpException('Invalid content (expected multipart/form-data [file] input)', HttpStatus.BAD_REQUEST);
          }
        const json = await this.parserService.parseWithSave(req.files.file.data);
        return json;
    } catch (e) {
        console.log(e);
        throw e;
    }
  }

}
