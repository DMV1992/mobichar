import { Injectable } from '@nestjs/common';
import stream = require('stream');
import AdmZip = require ('adm-zip');
import csv = require ('csv-parser');
import fs = require('fs');
import csvparser = require('csvtojson');
import { ResponseStructureDto } from './dto/ResponseStructureDto';
import _ = require('lodash');

@Injectable()
export class ParserService {
  async root() {
    return 'Hello World!';
  }

  async readAndParseFile(file) {
    try {
      let data = '';
      const zip = new AdmZip(file);
      // get info about file in zip
      const zipEntries = await zip.getEntries();
      const csvFiles = zipEntries.filter(f => f);
      zipEntries.forEach((zipEntry) => {
        // get string from csv for parsing
        if (zipEntry.entryName.includes('.csv')) {
          const csvString = zipEntry.getData().toString('utf8');
          data += csvString;
        }
        });
      const parseData = await csvparser({
          delimiter: '||',
        })
          .fromString(data)
          .on('csv',
            (row) => {
              console.log(row);
            });
      return parseData.map(row => new ResponseStructureDto(row));
    } catch (e) {
        throw e;
    }
  }

  // method for parsing csv and save file in temp folder
  async parseWithSave(data) {
    const fileNames = await this.unzipFile(data);
    const result = await this.parseFiles(fileNames);
    return result;
  }

  async unzipFile(file) {
    try {
      const zip = new AdmZip(file);
      const zipEntries = await zip.getEntries();
      // get only csv files
      const csvFiles = zipEntries.filter(ze => ze.entryName.includes('.csv'));
      await zip.extractAllTo('./temp', true);
      const names = csvFiles.map(f => f.entryName);
      return names;
    } catch (e) {
        throw e;
    }
}

  private readFile(fileName) {
    const row = [];
    return new Promise((resolve, reject) => {
      fs.createReadStream('./temp/' + fileName).pipe(csv({separator: '||'}))
      .on('data', (data) => {
        row.push(data);
      })
      .on('error', (err) => {
          // tslint:disable-next-line:no-console
          reject(err);
        })
      .on('end', (data) => {
          resolve(row);
        });
    });
  }

  // method for parse files without save in temp folder
  async parseFiles(names: string[]) {
    const res = names.map(n => this.readFile(n));
    const parseData = await Promise.all(res);
    const arr = await _.flattenDeep(parseData);
    return arr.map(row => new ResponseStructureDto(row));
  }

}
